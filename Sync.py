import todoist
import os
import time

# own imports
from Project import Project
from Item import Item
from Filter import Filter
from Label import Label


class Sync:

    """
    syncing all the objects of toDoist in the order
    of the equivalent dict below.
    in the future this needs to be reorganized for
    smart sync
    """

    projects = {}
    items = {}
    filters = {}
    labels = {}

    # email/passwd setup
    def __init__(self, *args, **kwargs):
        if len(kwargs) == 2:
            # call login routine
            self.login(kwargs['mail'], kwargs['passwd'])

            # backup login to file
            # THIS NEEDS TO BE ENCRYPTED FOR LOCAL STORE
            f = open('passwd', 'w+')
            f.write("%s %s" % (kwargs['mail'], kwargs['passwd']))
        elif len(kwargs) == 1:
            # call token routine
            self.token(kwargs['token'])

            # backup token to file
            # THIS NEEDS TO BE ENCRYPTED FOR LOCAL STORE
            f = open('token', 'w+')
            f.write("%s" % (kwargs['token']))
        else:
            print 'restore local settings'
            # restore local settings

            if os.path.isfile('token'):
                print 'via token'
                f = open('token')
                token = f.read()

                # restore data
                self.token(token)
            # restore login
            elif os.path.isfile('passwd'):
                print 'via login'
                f = open('passwd')
                mail, passwd = f.read().split()

                # restore data
                self.login(mail, passwd)

    def login(self, mail, passwd):
        # create api with email/passwd
        api = todoist.TodoistAPI()
        api.login(mail, passwd)

        # sync all items/projects/label
        self.syncDict = api.sync(resource_types=['all'])

    def token(self, token):
        # create api with access token
        api = todoist.TodoistAPI(token=token)

        # sync all items/projects/label
        self.syncDict = api.sync(resource_types=['all'])

    def fullDownSync(self):
        # sync the projects
        for i in self.syncDict['Projects']:
            print 'Project:', i['name']

            currProj = Project(i['archived_date'],
                               i['archived_timestamp'],
                               i['collapsed'],
                               i['color'],
                               i['id'],
                               i['indent'],
                               i['is_archived'],
                               i['is_deleted'],
                               i['item_order'],
                               i['name'],
                               i['shared'],
                               i['user_id'])
            currProjId = i['id']

            self.projects[currProjId] = currProj

        # sync items
        for i in self.syncDict['Items']:
            print 'Item:', i['content']

            currItems = Item(i['is_archived'],
                             i['labels'],
                             i['sync_id'],
                             i['in_history'],
                             i['checked'],
                             i['id'],
                             i['priority'],
                             i['user_id'],
                             i['date_added'],
                             i['content'],
                             i['item_order'],
                             i['project_id'],
                             i['date_string'],
                             i['due_date'],
                             i['day_order'],
                             i['assigned_by_uid'],
                             i['collapsed'],
                             i['indent'],
                             i['date_lang'],
                             i['is_deleted'],
                             i['due_date_utc'],
                             i['responsible_uid'])
            currItemsId = i['id']

            self.items[currItemsId] = currItems

        # sync filters
        for i in self.syncDict['Filters']:
            print 'Filter:', i['name']

            currFilter = Filter(i['color'],
                                ['id'],
                                ['is_deleted'],
                                ['item_order'],
                                ['name'],
                                ['query'],
                                ['user_id'])
            currFilterId = i['id']

            self.filters[currFilterId] = currFilter

        # sync labels
        for i in self.syncDict['Labels']:
            print 'Label:', i['name']

            currLabel = Label(i['is_deleted'],
                              i['uid'],
                              i['color'],
                              i['item_order'],
                              i['id'],
                              i['name'])
            currLabelId = i['id']

            self.labels[currLabelId] = currLabel

    def saveData():
        return None
