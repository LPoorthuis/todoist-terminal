

class Label(object):

    def __init__(self, is_deleted, uid, color, item_order, id_, name):
        self. is_deleted = is_deleted
        self. uid = uid
        self. color = color
        self.item_order = item_order
        self.id_ = id_
        self.name = name
