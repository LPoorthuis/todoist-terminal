from Sync import Sync
from Todos import Todos


def main():
    # sync API
    sync = Sync()
    sync.fullDownSync()

    # load tasks
    todos = Todos(projects=sync,
                  items=sync.items,
                  filters=sync.filters,
                  labels=sync.labels)
    todos.today()

if __name__ == '__main__':
    main()
