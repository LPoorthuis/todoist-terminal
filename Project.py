

class Project(object):

    items = []

    def __init__(self, archived_date, archived_timestamp, collapsed,
                 color, id_, indent, is_archived, is_deleted,
                 item_order, name, shared, user_id):
        self.archived_date = archived_date
        self.archived_timestamp = archived_timestamp
        self.collapsed = collapsed
        self.color = color
        self.id_ = id_
        self.indent = indent
        self.is_archived = is_archived
        self.is_deleted = is_deleted
        self.item_order = item_order
        self.name = name
        self.shared = shared
        self.user_id = user_id
