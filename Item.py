import time


class Item(object):

    def __init__(self, is_archived, labels, sync_id, in_history, checked,
                 id_, priority, user_id, date_added, content, item_order,
                 project_id, date_string, due_date, day_order, assigned_by_uid,
                 collapsed, indent, date_lang, is_deleted, due_date_utc,
                 responsible_uid):
        self.is_archived = is_archived
        self.labels = labels
        self.sync_id = sync_id
        self.in_history = in_history
        self.checked = checked
        self.id_ = id_
        self.priority = priority
        self.user_id = user_id
        self.date_added = date_added
        self.content = content
        self.item_order = item_order
        self.project_id = project_id
        self.date_string = date_string
        self.due_date = time.strptime(due_date[4:-7], '%d %b %Y %H:%M:%S') if due_date is not None else None
        self.day_order = day_order
        self.assigned_by_uid = assigned_by_uid
        self.collapsed = collapsed
        self.indent = indent
        self.date_lang = date_lang
        self.is_deleted = is_deleted
        self.due_date_utc = due_date
        self.responsible_uid = responsible_uid

        # date = time.strptime(i['due_date'][4:15], '%d %b %Y')
