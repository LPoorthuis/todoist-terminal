

class Filter(object):

    def __init__(self, color, id_, is_deleted, item_order,
                 name, query, user_id):
        self.color = color
        self.id_ = id_
        self.is_deleted = is_deleted
        self.item_order = item_order
        self.name = name
        self.query = query
        self.user_id = user_id
